from threading import Thread
from jetbot import Robot
import time
import datetime
import cv2
import RPi.GPIO as GPIO

ultra__trig = 12
ultra__echo = 24

def config_ultrasonic():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(ultra__trig, GPIO.OUT)
    GPIO.setup(ultra__echo, GPIO.IN)

def distance():
    while True:
        GPIO.output(ultra__trig, GPIO.HIGH)
        time.sleep(0.001)
        GPIO.output(ultra__trig, GPIO.LOW)

        #declare variables to avoid UnboundLocalError
        start_time = time.time()
        stop_time = time.time()
        while GPIO.input(ultra__echo) == 0:
            start_time = time.time()

        while GPIO.input(ultra__echo) == 1:
            stop_time = time.time()
        
        time_elapsed = stop_time - start_time
        distance = (time_elapsed * 34300) / 2
        time.sleep(0.5)

def motor(robot):
    while True:
        robot.left(speed=0.3)
        time.sleep(3)
        robot.left(speed=0.1)
        time.sleep(3)

def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1920,
    capture_height=1080,
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )
        
def config_camera():
    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    video_capture = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    return video_capture

def camera(video_capture):
    if video_capture.isOpened():
        try:
            while True:
                time0 = datetime.datetime.now()
                ret_val, frame = video_capture.read()
                if ret_val:
                    time1 = datetime.datetime.now()
                    time = time1 - time0
                    print("time for 1 frame: {} us".format(time.microseconds))                
        finally:
            video_capture.release()
    else:
        print("Error: unable to open camera")


if __name__ == "__main__":
    robot = Robot()
    config_ultrasonic()
    video_capture = config_camera()
    
    threadlist = []
    threadlist.append(Thread(target=distance))
    threadlist.append(Thread(target=motor, args=(robot,)))
    threadlist.append(Thread(target=camera, args=(video_capture,)))

    for t in threadlist:
        t.start()
    
    for t in threadlist:
        t.join()
