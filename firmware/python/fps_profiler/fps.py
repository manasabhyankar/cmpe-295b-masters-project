import cv2
import time
import datetime

def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1920,
    capture_height=1080,
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )
        
def config_camera():
    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    print(gstreamer_pipeline(flip_method=0))
    video_capture = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    return video_capture

def camera(video_capture):
    if video_capture.isOpened():
        try:
            while True:
                time0 = datetime.datetime.now()
                ret_val, frame = video_capture.read()
                time1 = datetime.datetime.now()
                time = time1 - time0
                print("time for 1 frame: {} us".format(time.microseconds)) 
        finally:
            video_capture.release()
    else:
        print("Error: unable to open camera")


if __name__ == "__main__":
    video_capture = config_camera()
    camera(video_capture)