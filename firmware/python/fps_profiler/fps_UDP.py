import cv2
import time
import socket
import pickle
import struct
import zlib
import datetime

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('98.37.180.15', 8485))  #165.227.20.225    10.0.0.185     98.37.180.15
connection = client_socket.makefile('wb')

def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1920,
    capture_height=1080,
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )
        
def config_camera():
    video_capture = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    return video_capture

def camera(video_capture):
    if video_capture.isOpened():
        try:
            while True:
                time0 = datetime.datetime.now()
                ret_val, frame = video_capture.read()
                if ret_val:
                    data = pickle.dumps(frame, 0)
                    size = len(data)    
                
                while (client_socket.sendall(struct.pack(">L", size) + data) != None):
                    i = i+1

                time1 = datetime.datetime.now()
                time = time1 - time0
                print("time for 1 frame: {} ms".format(time.microseconds))                    

        finally:
            video_capture.release()
    else:
        print("Error: unable to open camera")


if __name__ == "__main__":
    video_capture = config_camera()
    camera(video_capture)