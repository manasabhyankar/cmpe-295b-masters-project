'''

This python program is developed to do 3 tasks:
1. controls the motor speed & direction
2. takes the distance input (in cm) from the ultrasonic sensor
3. sends input sequence of frames of the MIPI CSI camera via UDP

'''

from threading import Thread
from jetbot import Robot
import time
import cv2
import RPi.GPIO as GPIO

import socket
import pickle
import zlib
import struct

#pin 12 & pin 24 of the ultrasonic sensor
ultra__trig = 12
ultra__echo = 24


client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('98.37.180.15', 8485))  #165.227.20.225    10.0.0.185     98.37.180.15
connection = client_socket.makefile('wb')

encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

def config_ultrasonic():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(ultra__trig, GPIO.OUT)
    GPIO.setup(ultra__echo, GPIO.IN)
    print("trig set as output pin; echo set as input pin")

def distance():
    while True:
        print("--------called function distance")
        GPIO.output(ultra__trig, GPIO.HIGH)
        time.sleep(0.001)
        GPIO.output(ultra__trig, GPIO.LOW)

        #declare variables to avoid UnboundLocalError
        start_time = time.time()
        stop_time = time.time()
        while GPIO.input(ultra__echo) == 0:
            start_time = time.time()

        while GPIO.input(ultra__echo) == 1:
            stop_time = time.time()
        
        time_elapsed = stop_time - start_time
        distance = (time_elapsed * 34300) / 2
        print(distance)
        time.sleep(0.5)

def motor(robot):
    while True:
        robot.left(speed=0.3)
        time.sleep(3)
        robot.left(speed=0.1)
        time.sleep(3)

def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1920,
    capture_height=1080,
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )
        
def config_camera():
    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    print(gstreamer_pipeline(flip_method=0))
    video_capture = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    return video_capture

def camera(video_capture):
    if video_capture.isOpened():
        img_counter = 0
        try:
            while True:
                ret_val, frame = video_capture.read()
                print("------------return value of video_capture.read()")
                print(ret_val)
                #print("------------value of frame")
                #print(frame)

                result, frame = cv2.imencode('.jpg', frame, encode_param)
                data = pickle.dumps(frame, 0)
                size = len(data)

                while (client_socket.sendall(struct.pack(">L", size) + data) != None):
                    i = 1 #dummy operation to keep the busy wait
                img_counter += 1
                print("{}: {}".format(img_counter, size))
                time.sleep(0.5)
        finally:
            video_capture.release()
    else:
        print("Error: unable to open camera")


if __name__ == "__main__":
    robot = Robot()
    config_ultrasonic()
    video_capture = config_camera()
    
    threadlist = []
    threadlist.append(Thread(target=distance))
    threadlist.append(Thread(target=motor, args=(robot,)))
    threadlist.append(Thread(target=camera, args=(video_capture,)))

    for t in threadlist:
        t.start()
    
    for t in threadlist:
        t.join()
