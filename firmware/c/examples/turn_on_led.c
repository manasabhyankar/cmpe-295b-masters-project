/*
    This is an example of using a switch to lit up an LED
    Pin7 is used to control the led
    Pin15 with a pull-up resistor is used to take the switch input
 */

#include "../gpio.h"

int main(void) {

    gpio_unexport(GPIO_PIN7);
    gpio_unexport(GPIO_PIN15);    
    
    gpio_export(GPIO_PIN15);
    gpio_set_direction(GPIO_PIN15, "in");

    gpio_export(GPIO_PIN7);
    gpio_set_direction(GPIO_PIN7, "out");
    gpio_set_pin_val(GPIO_PIN7, HIGH);

    //set pin 7 to 1 & 0 to change output voltage
    while(1) {
        if (gpio_get_pin_val(GPIO_PIN15) == LOW) {
             gpio_set_pin_val(GPIO_PIN7, HIGH);
            printf("Button detected low (released) LED is off \n");
        } 
        else if (gpio_get_pin_val(GPIO_PIN15) == HIGH) {
             gpio_set_pin_val(GPIO_PIN7, LOW);
            printf("Button detected high (pressed) LED is on \n");
        }
        usleep(1000000);
    }


    gpio_unexport(GPIO_PIN7);
    gpio_unexport(GPIO_PIN15);    
    return 0;
 
}