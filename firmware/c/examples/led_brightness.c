/*
    This is an example of using pwm to control the brightness of an led
    Pin32 is used to control the led
 */

#include "../pwm.h"

int main(void) {
    //int pwm = 0;

    pwm_export(PWM_PIN32);
    pwm_enable(PWM_PIN32);
    pwm_set_period(PWM_PIN32, 1000000);
    pwm_set_dutyCycle(PWM_PIN32, 500000);
    
    //toggle duty cycles between 100% & 70% (brightness)
    while(1) {
        pwm_set_dutyCycle(PWM_PIN32, 1000000);
	printf("100%% duty cycle \n");
        usleep(1000000); //suspend execution for n-us

        pwm_set_dutyCycle(PWM_PIN32, 700000);
	printf("70%% duty cycle \n");
        usleep(1000000); //suspend execution for n-us
    }

    pwm_disable(PWM_PIN32);
    pwm_unexport(PWM_PIN32);
    
    return 0;
}
