cmake_minimum_required(VERSION 3.10)

project(gpu_accel_vehicle VERSION 1.0)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# add the executable
add_executable(led_brightness 
led_brightness.c
../src/gpio.c
../src/pwm.c
)

add_executable(toggle_led 
toggle_led.c
../src/gpio.c
)

add_executable(turn_on_led 
turn_on_led.c
../src/gpio.c
)


add_executable(stepper_motor_dm542t_pwm 
stepper_motor_dm542t_pwm.c
../src/gpio.c 
../src/pwm.c
)
