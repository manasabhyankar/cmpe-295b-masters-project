from io import BytesIO
import socket
from PIL import Image
from PIL import ImageFile
import numpy as np
import sys
import cv2
import array
import LaneDetection

ImageFile.LOAD_TRUNCATED_IMAGES = True
lane_detect_object = LaneDetection.LaneDetection()

# Tuple of C# endpoint
loopback = ("127.0.0.1", 8000)
# State variable
state = "receiver"
# Create TCP socket to use for sending (and receiving)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.settimeout(30.0)
try:
    server.connect(loopback)
    print("Connection made")
except Exception as e:
    print(e)

"""

"""
data = 0
img_dat = 0
msg = 0
byte_arr = 0
image = 0
imcv = 0
edges = 0
flatten_edges = 0
dimensions = 0
data_second_round = 0
# Debug
time_step = 0
'''
'''

while True:
    if state == "receiver":
        data = server.recv(4)
        if(data):
            # print("Received value [{}] in bytes".format(int.from_bytes(data, sys.byteorder)))
            img_dat = server.recv(int.from_bytes(data, byteorder=sys.byteorder))
            state = server.recv(4).decode('ascii')
            # print(state)
            if state == "receiver":
                state = "send"
    elif state == "send":
        byte_arr = bytearray(img_dat)
        image = Image.open(BytesIO(byte_arr))
        imcv = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)
        #
        # Perform Canny edge detection
        edges = cv2.Canny(image=imcv, threshold1=100, threshold2=200)
        #
        success, temp = cv2.imencode(".png", edges)
        temp = np.array(temp)
        enc_img = temp.tobytes()
        enc_img_len = len(enc_img)
        #
        server.send(enc_img_len.to_bytes(4, sys.byteorder))
        server.send(enc_img)
        server.send(state.encode('ascii'))
        state = "receiver"
