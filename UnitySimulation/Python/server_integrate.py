import time
from io import BytesIO
import socket
import asyncio
from matplotlib.cbook import flatten
from PIL import Image
from PIL import ImageFile
import numpy as np
import sys
import cv2
import array
import LaneDetection

import pickle
import struct  ## new
import zlib

ImageFile.LOAD_TRUNCATED_IMAGES = True
lane_detect_object = LaneDetection.LaneDetection()
# *************************************************************************** Start: Connect to Jetbot
# Wait/Connect to remote device before trying to connect to Unity side
HOST = '10.0.0.185'
PORT = 8485

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Socket created')

s.bind((HOST, PORT))
print('Socket bind complete')
s.listen(10)
print('Socket now listening')

print("On host: ", socket.gethostname())
print("With IP: ", socket.gethostbyname(socket.gethostname()))

conn, addr = s.accept()

data = b""
payload_size = struct.calcsize(">L")
print("payload_size: {}".format(payload_size))
# *************************************************************************** End: Connect to Jetbot

# *************************************************************************** Start: Connect to Unity on port 8001
# Tuple of C# endpoint
loopback = ("127.0.0.1", 8001)
# Create TCP socket to use for sending (and receiving) from Unity
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.settimeout(30.0)
try:
    server.connect(loopback)
    print("Connected Unity")
except Exception as e:
    print(e)
# *************************************************************************** End: Connect to Unity on port 8001


# *************************************************************************** Start: Connect to Unity on port 8000
loopback_8000 = ("127.0.0.1", 8000)
# State variable
state = "receiver"
# Create TCP socket to use for sending (and receiving)
server_8000 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_8000.settimeout(30.0)
try:
    server_8000.connect(loopback_8000)
    print("Connection made")
except Exception as e:
    print(e)

# *************************************************************************** End: Connect to Unity on port 8000
"""

"""
img_dat = 0
msg = 0
byte_arr = 0
image = 0
imcv = 0
edges = 0
flatten_edges = 0
dimensions = 0
img_counter = 0
img_tag = 0
target_path = "C:\\Users\\1234r\\Documents\\test\\image_"
state = "receiver"
"""
"""

# Debug
time_step = 0
"""
"""

# FPS variables
prev_frame_time = 0
new_frame_time = 0
font = cv2.FONT_HERSHEY_SIMPLEX

while True:
    while len(data) < payload_size:
        print("Recv: {}".format(len(data)))
        data += conn.recv(4096)

    if state == "receiver":
        data_8000 = server_8000.recv(4)
        if data_8000:

            img_dat = server_8000.recv(int.from_bytes(data_8000, byteorder=sys.byteorder))
            state = server_8000.recv(4).decode('ascii')
            if state == "receiver":
                state = "send"

    elif state == "send":
        byte_arr = bytearray(img_dat)
        image_8000 = Image.open(BytesIO(byte_arr))
        imcv_8000 = cv2.cvtColor(np.asarray(image_8000), cv2.COLOR_RGB2BGR)
        #
        # Perform Canny edge detection
        edges_8000 = cv2.Canny(image=imcv_8000, threshold1=100, threshold2=200)
        #
        success, temp_8000 = cv2.imencode(".png", edges_8000)
        temp_8000 = np.array(temp_8000)
        enc_img_8000 = temp_8000.tobytes()
        enc_img_len_8000 = len(enc_img_8000)
        #
        server_8000.send(enc_img_len_8000.to_bytes(4, sys.byteorder))
        server_8000.send(enc_img_8000)
        server_8000.send(state.encode('ascii'))
        state = "receiver"

    print("Done Recv: {}".format(len(data)))
    packed_msg_size = data[:payload_size]
    data = data[payload_size:]
    msg_size = struct.unpack(">L", packed_msg_size)[0]
    print("msg_size: {}".format(msg_size))
    while len(data) < msg_size:
        data += conn.recv(4096)
    frame_data = data[:msg_size]
    data = data[msg_size:]

    frame = pickle.loads(frame_data, fix_imports=True, encoding="bytes")
    imcv = frame



    # # Calculate the incoming FPS
    # new_frame_time = time.time()
    # fps = 1/(new_frame_time-prev_frame_time)
    # prev_frame_time = new_frame_time
    # fps = int(fps)
    # fps = str(fps)
    # 
    # # putting the FPS count on the frame
    # cv2.putText(imcv, fps, (7, 70), font, 3, (100, 255, 0), 3, cv2.LINE_AA)
    # cv2.imshow("Incoming Frame", imcv)

    edges = lane_detect_object.process(imcv)

    # Perform Canny edge detection
    edges_canny = cv2.Canny(image=imcv, threshold1=100, threshold2=200)

    #print(edges_canny)

    success, temp = cv2.imencode(".png", edges_canny)

    temp = np.array(temp)
    enc_img = temp.tobytes()
    #print("Image length: ", len(enc_img))
    enc_img_len = len(enc_img)

    server.send(enc_img_len.to_bytes(4, sys.byteorder))
    server.send(enc_img)
    server.send(state.encode('ascii'))

    cv2.imshow('Feed', edges)
    cv2.waitKey(15)
