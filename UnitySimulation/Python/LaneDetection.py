import cv2
import numpy as np
import math


class LaneDetection:
    def __init__(self):
        # Modify to have no video object. Calling function with frame directly
        self.video = None

    def read(self):
        return self.video.read()

    def opened(self):
        return self.video.isOpened()

    def release(self):
        self.video.release()

    def make_points(self, image, average):
        slope, y_int = average
        y1 = image.shape[0]
        # how long we want our lines to be --> 3/5 the size of the image
        y2 = int(y1 * (3 / 5))
        # determine algebraically
        x1 = int((y1 - y_int) // slope)
        x2 = int((y2 - y_int) // slope)
        return np.array([x1, y1, x2, y2])

    # mask the image using the specified shape, in this case I used trapezoid shape
    def region_of_interest(self, img, vertices):
        mask = np.zeros_like(img)
        match_mask_color = 255
        cv2.fillPoly(mask, vertices, match_mask_color)
        masked_image = cv2.bitwise_and(img, mask)
        return masked_image

    # Draw detected lanes, including the region of area
    def draw_the_lines(self, img, lines):
        blank_image = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
        height = blank_image.shape[0]
        width = blank_image.shape[1]

        cv2.line(blank_image, (math.floor(width / 6), math.floor(height)),
                 (math.floor(width * 0.43), math.floor(height * 0.4)), (255, 0, 0), thickness=3)
        cv2.line(blank_image, (math.floor(width * 0.43), math.floor(height * 0.4)),
                 (math.floor(width * 0.47), math.floor(height * 0.4)), (255, 0, 0), thickness=3)
        cv2.line(blank_image, (math.floor(width * 0.47), math.floor(height * 0.4)),
                 (math.floor(width * .9), math.floor(height)), (255, 0, 0), thickness=3)
        cv2.line(blank_image, (math.floor(width * 0.9), math.floor(height)),
                 (math.floor(width / 6), math.floor(height)), (255, 0, 0), thickness=3)

        if lines is not None:
            try:  # Can get overflow error when trying to convert Python int to C long in function
                for line in lines:
                    x1, y1, x2, y2 = line
                    # draw lines on a black image
                    cv2.line(blank_image, (x1, y1), (x2, y2), (255, 255, 0), 10)
            except:
                print("Got overflow error")

        return blank_image

    # Calculate average lines of left and right.
    def average(self, image, lines):
        left = []
        right = []
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            parameters = np.polyfit((x1, x2), (y1, y2), 1)
            slope = parameters[0]
            y_int = parameters[1]
            if slope < 0:
                left.append((slope, y_int))
            else:
                right.append((slope, y_int))

        right_line = np.array([0, 0, -1, -1])
        left_line = np.array([0, 0, -1, -1])
        if (len(right) != 0):
            right_avg = np.average(right, axis=0)
            right_line = self.make_points(image, right_avg)

        if (len(left) != 0):
            left_avg = np.average(left, axis=0)
            left_line = self.make_points(image, left_avg)

        return np.array([left_line, right_line])

    # Calculate the two poins from slope and y-point
    # the calculated point will be used to draw the averaged lanes on each side

    # Process the each frame of the video.
    def process(self, image):
        height = image.shape[0]
        width = image.shape[1]
        region_of_interest_vertices = [
            (width / 6, height),
            (width * 0.43, height * 0.4),
            (width * 0.47, height * 0.4),
            (width * .9, height)
        ]

        # Converts to gray scale
        gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        # Converts to bgr color for hsv color image
        bgr_image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        hsv_image = cv2.cvtColor(bgr_image, cv2.COLOR_RGB2HSV)

        # Lower and Upper bound of yellow color
        lower_yellow = np.array([13, 100, 100], dtype="uint8")
        upper_yellow = np.array([30, 255, 255], dtype="uint8")

        # mask image with yellow and white color only
        mask_yellow = cv2.inRange(hsv_image, lower_yellow, upper_yellow)
        mask_white = cv2.inRange(gray_image, 150, 255)
        mask_yw = cv2.bitwise_or(mask_white, mask_yellow)
        mask_yw_image = cv2.bitwise_and(gray_image, mask_yw)

        # use Canny image detection.
        canny_image = cv2.Canny(mask_yw_image, 100, 200)

        # cropped with region of interest on canny image
        cropped_image = self.region_of_interest(canny_image,
                                                np.array([region_of_interest_vertices], np.int32), )

        # Detect the straight lines
        lines = cv2.HoughLinesP(cropped_image,
                                rho=2,
                                theta=np.pi / 180,
                                threshold=50,
                                lines=np.array([]),
                                minLineLength=20,
                                maxLineGap=10)
        if lines is not None:
            averaged_lines = self.average(image, lines)
            image_with_lines = self.draw_the_lines(image, averaged_lines)
            output_image = cv2.addWeighted(image, 0.8, image_with_lines, 1, 1)
        else:
            output_image = cropped_image
        return output_image
