from io import BytesIO
import socket
import asyncio
from matplotlib.cbook import flatten
from PIL import Image
from PIL import ImageFile
import numpy as np
import sys
import cv2
import array
import LaneDetection

ImageFile.LOAD_TRUNCATED_IMAGES = True
lane_detect_object = LaneDetection.LaneDetection()

# Tuple of C# endpoint
loopback = ("10.0.0.185", 8001)
# Create UDP socket to use for sending (and receiving)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.settimeout(30.0)
try:
    server.connect(loopback)
except Exception as e:
    print(e)

"""

"""
data = 0 
img_dat = 0
msg = 0
byte_arr = 0
image = 0
imcv = 0
edges = 0
flatten_edges = 0
dimensions = 0
img_counter = 0
img_tag = 0
target_path = "C:\\Users\\1234r\\Documents\\test\\image_"
state = "receiver"
cv2.namedWindow('Feed', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Feed', 1024, 768)
"""
"""

# Debug
time_step = 0
"""
"""
while True:
    time_step += 1
    if(state == "receiver"):
        data = server.recv(4)
        if(data):
            print("Received value [{}] in bytes".format(int.from_bytes(data, sys.byteorder)))
            img_dat = server.recv(int.from_bytes(data, byteorder=sys.byteorder))
            byte_arr = bytearray(img_dat)
            try:
                trans_array = array.array('f', byte_arr)
                float_array = trans_array.tolist()
                print("Incoming array: ")
                print(float_array)
                print("****************** Time step pass 1: ", time_step)
                data_second_round = server.recv(4)

            except:
                image = Image.open(BytesIO(byte_arr)).convert('RGB')
                #imcv = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)
                imcv = np.array(image)

                # # Perform Canny edge detection
                # Forgo canny and call lane detection library
                #edges = cv2.Canny(image=imcv, threshold1=100, threshold2=200)
                edges = lane_detect_object.process(imcv)
                flatten_edges = [byte for subarray in edges for byte in subarray]
                dimensions = len(edges) * len(edges[0])
                print("****************** Time step ***** picture pass 1: ", time_step)

        if (data_second_round):
            print("Received value [{}] in bytes".format(int.from_bytes(data_second_round, sys.byteorder)))
            img_dat = server.recv(int.from_bytes(data_second_round, byteorder=sys.byteorder))
            byte_arr = bytearray(img_dat)
            try:
                trans_array = array.array('f', byte_arr)
                float_array = trans_array.tolist()
                print("Incoming array: ")
                print(float_array)
                print("****************** Time step pass 2: ", time_step)

            except:
                image = Image.open(BytesIO(byte_arr)).convert('RGB')
                imcv = np.array(image)
                #imcv = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)

                # Forgo canny and call lane detection library
                # edges = cv2.Canny(image=imcv, threshold1=100, threshold2=200)
                edges = lane_detect_object.process(imcv)
                flatten_edges = [byte for subarray in edges for byte in subarray]
                dimensions = len(edges) * len(edges[0])
                print("****************** Time step ***** picture pass 2: ", time_step)
    cv2.imshow('Feed', edges)
    cv2.waitKey(1)
