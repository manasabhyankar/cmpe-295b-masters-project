from io import BytesIO
import socket
from PIL import Image
from PIL import ImageFile
import numpy as np
import sys
import cv2
import array

ImageFile.LOAD_TRUNCATED_IMAGES = True

# Tuple of C# endpoint
loopback = ("127.0.0.1", 8001)
# State variable
state = "receiver"
# Create UDP socket to use for sending (and receiving)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.settimeout(30.0)
try:
    server.connect(loopback)
except Exception as e:
    print(e)

cv2.namedWindow('Feed', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Feed', 1024, 768)
"""

"""
data = 0 
img_dat = 0
msg = 0
byte_arr = 0
image = 0
imcv = 0
edges = 0
flatten_edges = 0
dimensions = 0
"""
"""
while True:
        if(state == "receiver"):
            data = server.recv(4)
            if(data):
                print("Received value [{}] in bytes".format(int.from_bytes(data, sys.byteorder)))
                img_dat = server.recv(int.from_bytes(data, byteorder=sys.byteorder))
                byte_arr = bytearray(img_dat)
                try:
                    trans_array = array.array('f', byte_arr)
                    float_array = trans_array.tolist()
                    print("Incoming array: ")
                    print(float_array)

                except e:
                    image = Image.open(BytesIO(byte_arr))
                    imcv = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)

                    # # Perform Canny edge detection
                    edges = cv2.Canny(image=imcv, threshold1=100, threshold2=200)
                    flatten_edges = [byte for subarray in edges for byte in subarray]
                    dimensions = len(edges) * len(edges[0])
        cv2.imshow('Feed', edges)
        cv2.waitKey(1)
