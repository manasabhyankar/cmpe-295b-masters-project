using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Drawing;
using Assets.Scripts;

public class SaveCamera : MonoBehaviour

{
    public Camera MainCamera; // Define camera object that will be used to get Main Camera's info
    public Texture2D image;
    public Texture2D texture;
    public bool enter = false;
    //Thread receiveThread; // Receiving Thread
    Server server;
    byte[] bytes;
    string state = "receive";
    // Start is called before the first frame update
    void Start()
    {
        MainCamera = GetComponent<Camera>();
    }
    
    void Awake()
    {
        texture = new Texture2D(1, 1);
        server = new Server();
        server.Start();
    }
    
    
    private void ProcessInput(byte[] input)
    {
        // PROCESS INPUT RECEIVED ARRAY HERE

    }
    
    //Prevent crashes - close clients and threads properly!
    void OnDisable()
    {
        server.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (MainCamera != null) // If we have main camera feed
        {
            if (enter == false)
            {
                Debug.Log("First time entering");
                enter = true;
            }
            RenderTexture activeRenderTexture = RenderTexture.active;
            RenderTexture.active = MainCamera.targetTexture;

            if (MainCamera.targetTexture == null) // If no texture assigned to main camera, then create new RenderTexture
            {
                MainCamera.targetTexture = new RenderTexture(140, 140, 24);
            }

            MainCamera.Render(); // Render the main camera output

            // TODO: Overcome race condition when putting image initialization code in Awake or Start function
            image = new Texture2D(MainCamera.targetTexture.width, MainCamera.targetTexture.height); // Create new 2D image texture with Main camera's dimensions

            image.ReadPixels(new Rect(0, 0, MainCamera.targetTexture.width, MainCamera.targetTexture.height), 0, 0);
            image.Apply();
            RenderTexture.active = activeRenderTexture;

            bytes = image.EncodeToPNG();
            int byte_len = 7000;

            // if (server.CanSend() && TimeManager.raceStarted)
            // {
            //     server.SendImage(BitConverter.GetBytes(byte_len), bytes);
            // }
            if (server.CanReceive() && TimeManager.raceStarted)
            {
                server.ReceiveImage();
                texture = new Texture2D(140, 140);
                texture.LoadImage(server.GetImagePacket());
                texture.Apply();
                // byte[] byte_tex = texture.EncodeToPNG();
                // var dirPath = Application.dataPath + "/../SavedImage";
                // if(!Directory.Exists(dirPath)) {
                //     Directory.CreateDirectory(dirPath);
                // }
                // File.WriteAllBytes(dirPath + "Image" + ".png", byte_tex);
                GameObject.Find("Cube").GetComponent<Renderer>().material.mainTexture = texture;
            }
        }
    }
    
    // Update that has the frequency of the underlying physics system. Called at every fixed frame-rate frame.
    void FixedUpdate()
    {
        
        

    }

    void lateUpdate()
    {
    }
    
}
