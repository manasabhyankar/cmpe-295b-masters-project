﻿using UnityEngine;

namespace KartGame.KartSystems {

    public class KeyboardInput : BaseInput
    {
        public string TurnInputName = "Horizontal";
        public string AccelerateButtonName = "Accelerate";
        public string BrakeButtonName = "Brake";

        public override InputData GenerateInput() {
            //GameObject go = GameObject.Find("KartClassic_Player");
           // UdpSocket udpInfo = go.GetComponent <UdpSocket> ();
            //float horizon = udpInfo.horizon;
            return new InputData
            {
                Accelerate = Input.GetButton(AccelerateButtonName),
                Brake = Input.GetButton(BrakeButtonName),
                TurnInput = Input.GetAxis("Horizontal")
                //TurnInput = horizon
                
            };
        }
    }
}
