﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Net;
using UnityEngine;

namespace Assets.Scripts.AI
{
    public class Server
    {
        public Server()
        {
            port = 8000;
            ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            packet_size = new byte[4] { 0x00, 0x00, 0x00, 0x00 };
        }


        public string state = "send";
        private Socket listener;
        private Socket client;
        private EndPoint ep;
        private int port;
        private byte[] image_packet;
        private byte[] packet_size;
        private byte[] state_arr;
        int byte_len = 0;
        int image_count = 0;
        int skip_count = 0;
        public void Start()
        {
            listener.Bind(ep);
            listener.Listen(1);
            Debug.Log($"Server is listening on port {port}.");
            client = listener.Accept();
            Debug.Log($"Received connection on port {port}.");
        }
        
        public byte[] GetImagePacket()
        {
            return image_packet;
        }

        public void Stop()
        {
            client.Close();
            listener.Close();
        }

        public bool CanSend()
        {
            return client.Connected && (state == "send");
        }

        public bool CanReceive()
        {
            return Convert.ToBoolean(client.Available) && (state == "receive");
        }

        private void SendState()
        {
            byte[] str_bytes = Encoding.ASCII.GetBytes(state);
            //SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            //e.SetBuffer(str_bytes, 0, str_bytes.Length);
            //e.Completed += new EventHandler<SocketAsyncEventArgs>(SendHandler);
            Debug.Log($"Byte length of state: {str_bytes.Length}");
            try
            {
                //client.SendAsync(e);
                client.Send(str_bytes, 0, 4, SocketFlags.None);
                state = "receive";
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }



        private void SendImagePacketLength(byte[] data)
        {
            //SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            //e.SetBuffer(data, 0, data.Length);
            //e.Completed += new EventHandler<SocketAsyncEventArgs>(SendHandler);
            Debug.Log($"Byte length of image packet length: {data.Length}");
            try
            {
                client.Send(data, 0, 4, SocketFlags.None);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }

        private void SendImagePacket(byte[] data)
        {
            //SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            //e.SetBuffer(data, 0, data.Length);
            //e.Completed += new EventHandler<SocketAsyncEventArgs>(SendHandler);
            Debug.Log($"Byte length of image packet: {data.Length}");
            try
            {
                client.Send(data, 0, data.Length, SocketFlags.None);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }

        public async void SendImage(byte[] data1, byte[] data2)
        {
            await Task.Factory.StartNew(() => SendImagePacketLength(data1))
                .ContinueWith(SendImagePacketLength => SendImagePacket(data2))
                .ContinueWith(SendImagePacket => SendState());
        }

        private void SendHandler(object sender, SocketAsyncEventArgs e)
        {
            Debug.Log($"weh");
        }

        private void ReceiveState()
        {
            state_arr = new byte[10];
            //client.BeginReceive(state_arr, 0, 4, SocketFlags.None, ReceiveStateHandler, null);
            try
            {
                client.Receive(state_arr, 0, 10, SocketFlags.None);
                Debug.Log($"Received state value: {Encoding.ASCII.GetString(state_arr)}");
                state = "send"; // Encoding.ASCII.GetString(state_arr);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }

        private void ReceiveImagePacketLength()
        {
            try
            {
                //client.BeginReceive(packet_size, 0, 4, SocketFlags.None, ReceiveImagePacketLengthHandler, null);
                byte[] temp = new byte[4];
                client.Receive(temp, 0, 4, SocketFlags.None);
                byte_len = BitConverter.ToInt32(temp, 0);
                Debug.Log($"Received image packet length: {byte_len}");
            }
            catch(Exception ex)
            {
                Debug.Log(ex);
            }
        }

        private void ReceiveImagePacket()
        {
            try
            {
                //client.BeginReceive(image_packet, 0, byte_len, SocketFlags.None, ReceiveImagePacketHandler, null);
                image_packet = new byte[byte_len];
                client.Receive(image_packet, 0, byte_len, SocketFlags.None);
                Debug.Log($"Received image packet of size: {image_packet.Length}");
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }

        private void ApplyTexture()
        {

        }


        public async void ReceiveImage()
        {
            await Task.Factory.StartNew(() => ReceiveImagePacketLength())
                .ContinueWith(ReceiveImagePacketLength => ReceiveImagePacket())
                .ContinueWith(ReceiveImagePacket => ReceiveState());
                //.ContinueWith(ReceiveImagePacket => ImageSave());
        }

        public async void ImageSave()
        {
            skip_count++;
            if(skip_count % 5 == 0)
            {
                await Task.Run(() =>
                {
                    File.WriteAllBytes($"C:\\Users\\1234r\\Documents\\test\\image_{image_count.ToString()}.png", image_packet);
                    image_count++;
                });
            }

            //using (Image x = Image.FromStream(new MemoryStream(image_packet)))
            //{
            //    await Task.Run(() =>
            //    {
            //        x.Save(@"C:\Users\1234r\Documents\test\image_" + @image_count.ToString() + @".png", System.Drawing.Imaging.ImageFormat.Png);
            //    });
            //}
            //img.Save("C:\\Users\\1234r\\Documents\\test\\image_" + image_count.ToString(), System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}
